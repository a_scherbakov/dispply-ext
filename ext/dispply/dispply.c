#include <ruby.h>
#include <ruby/encoding.h>

VALUE           mDispply;
VALUE           mDispplyEnc;

static inline int
single_byte_optimizable(VALUE str)
{
    rb_encoding    *enc;

    /*
     * Conservative.  It may be ENC_CODERANGE_UNKNOWN. 
     */
    if (ENC_CODERANGE(str) == ENC_CODERANGE_7BIT)
	return 1;

    enc = rb_enc_get(str);
    if (rb_enc_mbmaxlen(enc) == 1)
	return 1;

    /*
     * Conservative.  Possibly single byte. "\xa1" in Shift_JIS for
     * example. 
     */
    return 0;
}

VALUE
enc_swap(VALUE self, VALUE str)
{
    rb_encoding    *enc;
    char           *s,
                   *e,
                   *p,
                    ch,
                    buf[6];
    int             cr,
                    i;
    VALUE           rev;

    // rev = rb_str_dup(str);
    rev = rb_str_new_with_class(str, 0, RSTRING_LEN(str));
    Check_Type(str, T_STRING);
    enc = rb_enc_get(str);

    if (RSTRING_LEN(str) <= 1)
	return str;

    s = RSTRING_PTR(str);
    e = RSTRING_END(str);
    p = RSTRING_PTR(rev);

    memcpy(p, s, RSTRING_LEN(str));

    if (single_byte_optimizable(str)) {
	int             steps = (RSTRING_LEN(str) / 2 - 1);
	// printf("STEPS %d\n", steps);
	for (i = 0; i < RSTRING_LEN(str) - 1; i += 2) {
	    // printf("STEPS %d %d\n", steps, i);
	    p[i] = s[i + 1];
	    p[i + 1] = s[i];
	}
    } else {
	cr = rb_enc_asciicompat(enc) ?
	    ENC_CODERANGE_7BIT : ENC_CODERANGE_VALID;
	while (s < e) {
	    int             clen1 = rb_enc_fast_mbclen(s, e, enc);
	    int             clen2 = 0;
	    if (s + clen1 < e) {
		clen2 = rb_enc_fast_mbclen(s + clen1, e, enc);

		memcpy(p, s + clen1, clen2);
		memcpy(p + clen1, s, clen1);

	    } else {
		memcpy(p, s, clen1);
	    }
	    p += clen1 + clen2;
	    s += clen1 + clen2;
	}
    }

    rb_str_set_len(rev, RSTRING_LEN(str));
    rb_obj_infect(rev, str);
    rb_enc_copy(rev, str);
    ENC_CODERANGE_SET(rev, cr);

    return rev;
}

VALUE
enc_shift(VALUE self, VALUE str)
{
    rb_encoding    *enc;
    char           *s,
                   *e,
                   *p,
                    ch,
                    buf[6];
    int             cr,
                    i;
    VALUE           rev;

    // rev = rb_str_dup(str);
    rev = rb_str_new_with_class(str, 0, RSTRING_LEN(str));
    Check_Type(str, T_STRING);
    enc = rb_enc_get(str);

    if (RSTRING_LEN(str) <= 1)
	return str;

    s = RSTRING_PTR(str);
    e = RSTRING_END(str);
    p = RSTRING_PTR(rev);

    for (i = 0; i < RSTRING_LEN(str); ++i) {
	ch = s[i];
	ch += 3;
	if (ch > 127)
	    ch -= 128;
	p[i] = ch;
    }

    rb_str_set_len(rev, RSTRING_LEN(str));
    rb_obj_infect(rev, str);
    rb_enc_copy(rev, str);
    ENC_CODERANGE_SET(rev, cr);

    return rev;
}

void
Init_dispply()
{
    mDispply = rb_define_module("Dispply");
    mDispplyEnc = rb_define_module_under(mDispply, "Encode");

    rb_define_module_function(mDispplyEnc, "swap", enc_swap, 1);
    rb_define_module_function(mDispplyEnc, "shift", enc_shift, 1);
}
