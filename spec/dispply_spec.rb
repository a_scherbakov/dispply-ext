#encoding: utf-8
require 'spec_helper'

describe Dispply do
  it 'has a version number' do
    expect(Dispply::VERSION).not_to be nil
  end

  describe Dispply::Encode do
    context 'swap char in string' do
      let(:control_func) do
        lambda do |s|
          for i in (0..s.length/2-1)
            s[i*2],s[i*2+1] = s[i*2+1],s[i*2]
          end
          s
        end
      end

      %w(qwerty 123455 wqeqwe цукйцук).each do |str|
        it do
          expect(Dispply::Encode.swap(str)).to eq(control_func.call(str))
        end
      end
    end

    context 'shift charcode in string' do
      let(:control_func) do
        lambda do |s|
          result = ''
          s.each_byte { |b|
            b += 3
            b-=128 if b>127
            result += b.chr
          }
          result
        end
      end

      %w(qwerty 123455 wqeqwe).each do |str|
        it do
          expect(Dispply::Encode.shift(str)).to eq(control_func.call(str))
        end
      end
    end
  end
end
